import React, {useEffect, useState} from "react";

import Input from "../Input";
import Link from "../Link";
import Button from "../Button";

import "./SignInPage.css";

const SignInPage = () => {
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(`Login: ${login}  Password: ${password}`);
    }

    return (
        <form onSubmit={() => handleSubmit()}>
            <Input type={"text"} value={login} placeholder={"Username"} changeInputValue={setLogin} />
            <Input type={"password"} value={password} placeholder={"Password"} changeInputValue={setPassword} />
            <div>
                <p className="form__link">
                    Forgot
                    <Link link={'#'} classNames={""}>Username / Password?</Link>
                </p>
            </div>
            <Button type={"submit"} onClickAction={() => {}}>sign in</Button>
        </form>
    );
}

export default SignInPage;