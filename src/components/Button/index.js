import React from "react";

import "./Button.css";

const Button = ({ children, onClickAction, type, classNames }) => (
    <button type={type}
            onClick={onClickAction}
            className={`${classNames}`}
    >
        {children}
    </button>
);

export default Button;