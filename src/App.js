import React from "react";

import Header from "./components/Header";
import SignInPage from "./components/SignInPage";
import Footer from "./components/Footer";

import './App.css';

const App = () => (
      <div className="app">
          <Header text={"Sign In"} classNames={""} />
          <SignInPage />
          <Footer subText={"Don't have an account?"} actionText={"sign up now"} />
      </div>
);

export default App;
